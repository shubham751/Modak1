package com.example.modak.Model

import android.widget.ImageView

data class ItemDetailModel(val image : Int,val name : String,val rating : Float, val desc : String) {
}